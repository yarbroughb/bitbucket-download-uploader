# Bitbucket Download Uploader

Bitbucket Download Uploader (bbdu) is a simple script that allows you to more
easily upload files to a repository's list of downloads.

It was created for use within Bitbucket Pipelines to facilitate uploading build
artifacts more easily, but can also be used in any other CI or locally.

## Usage

### Required Values

This script requires the following values in order to work:

   1. The owner of the repository you intend to upload to.

      This can be specified by using the `-o` or `--owner` flags or from the
	  environment using the variable `BITBUCKET_REPO_OWNER`.

   2. The slug of the repository you intend to upload to.

      This can be specified by using the `-r` or `--repo` flags or from the
	  environment using the variable `BITBUCKET_REPO_SLUG`.

   3. A user to upload the file(s) as.

      By default the user will be the same as the owner; however, you can
	  specify a different user by using the `-u` or `--user` flags or from the
	  environment using the variable `BITBUCKET_DOWNLOADS_USER`.

   4. A password for the user doing the uploads.

      This can be specified by using the `-p` or `--pwd` flags or from the
	  environment using the variable `BITBUCKET_DOWNLOADS_PWD`.

	  *It is strongly recommended that you use an app password instead of your
	  account password!*

   5. The file(s) to be uploaded.

      These files are specified via the command line after all flags.

**Note:** Values passed in via the command line take precedence over environment
          variables.

### Examples

#### Syntax

```````````````````````````````````````````````````````````````````````````shell

bbdu --owner OWNER --repo REPO --pwd PASSWORD FILE1 FILE2 FILE3

```````````````````````````````````````````````````````````````````````````

The above is equivalent to:

```````````````````````````````````````````````````````````````````````````shell

bbdu -o OWNER -r REPO -p PASSWORD FILE1 FILE2 FILE3

```````````````````````````````````````````````````````````````````````````

#### Uploading a Single File

To upload this [README](README.md) to this repository's downloads, you would
do the following:

```````````````````````````````````````````````````````````````````````````shell

bbdu --owner yarbroughb --repo bitbucket-download-uploader --pwd PASSWORD README.md

```````````````````````````````````````````````````````````````````````````

#### Uploading Multiple Files

To upload this [README](README.md) and the [LICENSE](LICENSE) to this
repository's downloads, you would do the following:

```````````````````````````````````````````````````````````````````````````shell

bbdu --owner yarbroughb --repo bitbucket-download-uploader --pwd PASSWORD README.md LICENSE

```````````````````````````````````````````````````````````````````````````

#### Uploading Files Using Wildcards

You may also use wildcards to specify one or more files.

For example, if you want to upload all `.zip` files in the current directory to
this repository's downloads, you would do the following:

```````````````````````````````````````````````````````````````````````````shell

bbdu --owner yarbroughb --repo bitbucket-download-uploader --pwd PASSWORD *.zip

```````````````````````````````````````````````````````````````````````````

#### Uploading Files As Another User

The previous examples all assume you want to upload the file as the owner of the
repository, but you may upload them using any account that has write access to
the repository.

For example, if you want to repeat the last example except with "someuser"
listed as the uploader, you would do the following:

```````````````````````````````````````````````````````````````````````````shell

bbdu --owner yarbroughb --repo bitbucket-download-uploader --user someuser --pwd PASSWORD *.zip

```````````````````````````````````````````````````````````````````````````
**Note:** The supplied password must be valid for the user "someuser", a
          password belonging to the owner will not work!

### Usage in Bitbucket Pipelines

When used in Bitbucket Pipelines, this script will detect the repository owner
and slug of the current repository from the environment.  This means that if
you are uploading to the same repository that triggered the build, you do not
need to specify them.

It is also recommended that you set your password in the environment variable
`BITBUCKET_DOWNLOADS_PWD` to keep it hidden and to have it be automatically
detected.

Assuming you are uploading to the same repository and have set your password in
the environment variable `BITBUCKET_DOWNLOADS_PWD`, the syntax can be simplified
to just:

```````````````````````````````````````````````````````````````````````````shell

bbdu FILE1 FILE2 FILE3

```````````````````````````````````````````````````````````````````````````

## License

This project is licensed under the [MIT License](LICENSE).
